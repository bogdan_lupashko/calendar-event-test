 **The Task:**

`Using Restful style, store data (Account, Name, Language, Date Format, Time Format, Time Zone) via http request,
process response - write to MySql database via crud. Write a script to create a table. 
Methods must support search, pagination, dynamic data search (number of criteria and sample size varies).
Code should be covered by tests. Validate JSR-303. 
The API must be covered by Swagger. P.S. Don't forget to open access to the repository.`


**Run IT and Unit tests**

`mvn clean verify`

**Author**

`bogdanlupashko@gmail.com`

