package org.bl.company.test.calendar.controller;

import lombok.SneakyThrows;
import org.bl.company.test.calendar.BaseIntegration;
import org.bl.company.test.calendar.service.dto.Event;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EventControllerIT extends BaseIntegration {

    private static final String BASE_PATH = "/api/calendar/event";
    private static final String SEARCH_PATH = BASE_PATH + "/search";

    @Test
    @SneakyThrows
    public void search_positive() {
        mockMvc.perform(get(SEARCH_PATH)
                        .param("page", "0")
                        .param("numOfItems", "3")
                        .param("timeZone", "UTC")
                        .contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.content").isNotEmpty())
                .andExpect(jsonPath("$.content[0].account").value("first1"))
                .andExpect(jsonPath("$.content[0].name").value("first1"))
                .andExpect(jsonPath("$.content[0].language").value("en-gb"))
                .andExpect(jsonPath("$.content[0].dateFormat").value("mm/dd/yyyy"))
                .andExpect(jsonPath("$.content[0].timeFormat").value("HH:mm:ss"))
                .andExpect(jsonPath("$.content[0].timeZone").value("UTC"));

    }

    @Test
    @SneakyThrows
    public void searchWithoutParams_emptyResult() {
        mockMvc.perform(get(SEARCH_PATH)
                        .param("page", "0")
                        .param("numOfItems", "3")
                        .contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.content").isEmpty());

    }

    @Test
    @SneakyThrows
    public void searchPageSizeZero_exception() {
        mockMvc.perform(get(SEARCH_PATH)
                        .param("page", "0")
                        .param("numOfItems", "0")
                        .param("timeZone", "UTC")
                        .contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(500))
                .andExpect(jsonPath("$").value("search.numOfItems: must be greater than or equal to 1"));

    }

    @Test
    @SneakyThrows
    public void searchPageNumNegative_exception() {

        mockMvc.perform(get(SEARCH_PATH)
                        .param("page", "-1")
                        .param("numOfItems", "2")
                        .param("timeZone", "UTC")
                        .contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(500))
                .andExpect(jsonPath("$").value("Page index must not be less than zero!"));

    }

    @Test
    @SneakyThrows
    public void create_positive() {

        var event = Event.builder()
                .account("first5")
                .name("first5")
                .language("en-ca")
                .dateFormat("mm/dd/yy")
                .timeFormat("hh:mm aa")
                .timeZone("CST")
                .build();
        var json = objectMapper.writeValueAsString(event);

        mockMvc.perform(post(BASE_PATH)
                        .content(json)
                        .contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(201))
                .andExpect(jsonPath("$.account").value("first5"))
                .andExpect(jsonPath("$.name").value("first5"))
                .andExpect(jsonPath("$.language").value("en-ca"))
                .andExpect(jsonPath("$.dateFormat").value("mm/dd/yy"))
                .andExpect(jsonPath("$.timeFormat").value("hh:mm aa"))
                .andExpect(jsonPath("$.timeZone").value("CST"));
    }

    @Test
    @SneakyThrows
    public void createEmptyFields_exception() {

        var event = new Event();
        var json = objectMapper.writeValueAsString(event);

        mockMvc.perform(post(BASE_PATH)
                        .content(json)
                        .contentType(APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.errors",
                        Matchers.containsInAnyOrder(
                                "Please provide a timeFormat",
                                "Please provide a dateFormat",
                                "Please provide a language",
                                "Please provide a timeZone",
                                "Please provide a name",
                                "Please provide a account")));
    }

}