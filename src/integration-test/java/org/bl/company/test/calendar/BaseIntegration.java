package org.bl.company.test.calendar;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.utility.DockerImageName;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class BaseIntegration {
    protected MockMvc mockMvc;

    @Rule
    @SuppressWarnings("rawtypes")
    public GenericContainer mysql = new MySQLContainer<>(DockerImageName.parse("mysql:8.0.25"))
            .withExposedPorts(3306);
    @Autowired
    public WebApplicationContext webAppContext;
    @Autowired
    public ObjectMapper objectMapper;

    @Before
    public void before() {
        mockMvc = webAppContextSetup(webAppContext).build();
        mysql.start();
    }

    @After
    public void after() {
        mysql.stop();
    }

}
