package org.bl.company.test.calendar.service.impl;

import org.bl.company.test.calendar.Application;
import org.bl.company.test.calendar.exception.GenericException;
import org.bl.company.test.calendar.repo.EventRepository;
import org.bl.company.test.calendar.repo.entity.EventEntity;
import org.bl.company.test.calendar.service.dto.Event;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.doReturn;

@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
public class EventServiceImplTest {

    @MockBean
    private EventRepository eventRepository;
    @Autowired
    private EventServiceImpl eventService;

    @Test
    @SuppressWarnings("unchecked")
    public void searchCalendar_positive() {
        var eventEntity = getEventEntity();

        doReturn(new PageImpl<>(Collections.singletonList(eventEntity)))
                .when(eventRepository).findAll(nullable(Specification.class), nullable(Pageable.class));

        var result = eventService.searchCalendar(new Event(), Pageable.ofSize(5));
        assertNotNull(result);
        assertEquals(1, result.getSize());
        assertTrue(result.get().findFirst().isPresent());

        var eventResult = result.get().findFirst().get();
        assertEquals(eventEntity.getAccount(), eventResult.getAccount());
        assertEquals(eventEntity.getLanguage(), eventResult.getLanguage());
        assertEquals(eventEntity.getDateFormat(), eventResult.getDateFormat());
        assertEquals(eventEntity.getTimeFormat(), eventResult.getTimeFormat());
        assertEquals(eventEntity.getTimeZone(), eventResult.getTimeZone());
        assertEquals(eventEntity.getName(), eventResult.getName());

    }

    @Test(expected = GenericException.class)
    @SuppressWarnings("unchecked")
    public void searchCalendar_exception() {
        var event = new Event();

        doReturn(null)
                .when(eventRepository).findAll(nullable(Specification.class), nullable(Pageable.class));

        eventService.searchCalendar(event, Pageable.ofSize(5));

    }

    @Test
    public void create_positive() {

        var eventEntity = getEventEntity();

        doReturn(eventEntity)
                .when(eventRepository).save(nullable(EventEntity.class));

        var result = eventService.create(new Event());
        assertNotNull(result);
        assertEquals(eventEntity.getAccount(), result.getAccount());
        assertEquals(eventEntity.getLanguage(), result.getLanguage());
        assertEquals(eventEntity.getDateFormat(), result.getDateFormat());
        assertEquals(eventEntity.getTimeFormat(), result.getTimeFormat());
        assertEquals(eventEntity.getTimeZone(), result.getTimeZone());
        assertEquals(eventEntity.getName(), result.getName());

    }

    @Test(expected = GenericException.class)
    public void create_exception() {

        doReturn(null)
                .when(eventRepository).save(nullable(EventEntity.class));

        eventService.create(new Event());
    }

    private EventEntity getEventEntity() {
        return EventEntity.builder()
                .account("first5")
                .dateFormat("mm/dd/yy")
                .timeFormat("hh:mm aa")
                .timeZone("CST")
                .language("en-ca")
                .name("first5")
                .build();
    }
}