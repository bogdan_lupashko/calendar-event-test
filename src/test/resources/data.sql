DROP TABLE IF EXISTS events;

CREATE TABLE events
(
    id          CHARACTER VARYING(36) NOT NULL,
    account     VARCHAR(512),
    name        VARCHAR(512),
    language    VARCHAR(512),
    date_format VARCHAR(512),
    time_format VARCHAR(512),
    time_zone   VARCHAR(512),

    PRIMARY KEY (id)
);

INSERT INTO events (id, account, name, language, date_format, time_format, time_zone)
VALUES ('eqwrt1', 'first1', 'first1', 'en-gb', 'mm/dd/yyyy', 'HH:mm:ss', 'UTC'),
       ('eqwrt2', 'first2', 'first2', 'en-us', 'mm/dd/yyyy', 'hh:mm:ss aa', 'CST'),
       ('eqwrt3', 'first3', 'first3', 'uk-ua', 'dd/mm/yyyy', 'HH:mm:ss', 'GMT+03:00');
