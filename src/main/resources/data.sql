DROP TABLE IF EXISTS calendars;

CREATE TABLE calendars
(
    id          CHARACTER VARYING(36) NOT NULL,
    account     VARCHAR(512),
    name        VARCHAR(512),
    language    VARCHAR(512),
    date_format VARCHAR(512),
    time_format VARCHAR(512),
    time_zone   VARCHAR(512),

    PRIMARY KEY (id)
);