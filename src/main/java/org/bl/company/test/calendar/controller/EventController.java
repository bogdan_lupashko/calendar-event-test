package org.bl.company.test.calendar.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bl.company.test.calendar.service.EventService;
import org.bl.company.test.calendar.service.dto.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@Slf4j
@Api(tags = "Calendar events")
@RequestMapping("/api/calendar/event")
@Validated
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class EventController {

    private final EventService eventService;

    @GetMapping(value = "/search", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Calendar event search", response = Event.class)
    public ResponseEntity<Page<Event>> search(@RequestParam(value = "page") int page,
                                              @Min(1) @RequestParam(value = "numOfItems") int numOfItems,
                                              @RequestParam(value = "account", required = false) String account,
                                              @RequestParam(value = "name", required = false) String name,
                                              @RequestParam(value = "language", required = false) String language,
                                              @RequestParam(value = "dateFormat", required = false) String dateFormat,
                                              @RequestParam(value = "timeFormat", required = false) String timeFormat,
                                              @RequestParam(value = "timeZone", required = false) String timeZone) {

        final var calendarFilter = Event.builder()
                .account(account)
                .name(name)
                .language(language)
                .dateFormat(dateFormat)
                .timeFormat(timeFormat)
                .timeZone(timeZone)
                .build();

        return new ResponseEntity<>(
                eventService.searchCalendar(calendarFilter, PageRequest.of(page, numOfItems)),
                HttpStatus.OK);

    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Calendar event create", response = Event.class)
    public ResponseEntity<Event> create(@Valid @RequestBody Event event) {
        return new ResponseEntity<>(
                eventService.create(event),
                HttpStatus.CREATED);
    }

}

