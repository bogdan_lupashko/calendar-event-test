package org.bl.company.test.calendar.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Event {

    @NotEmpty(message = "Please provide a account")
    private String account;
    @NotEmpty(message = "Please provide a name")
    private String name;
    @NotEmpty(message = "Please provide a language")
    private String language;
    @NotEmpty(message = "Please provide a dateFormat")
    private String dateFormat;
    @NotEmpty(message = "Please provide a timeFormat")
    private String timeFormat;
    @NotEmpty(message = "Please provide a timeZone")
    private String timeZone;
}
