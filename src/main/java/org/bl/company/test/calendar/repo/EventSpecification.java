package org.bl.company.test.calendar.repo;

import org.bl.company.test.calendar.repo.entity.EventEntity;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class EventSpecification implements Specification<EventEntity> {

    private final EventEntity filter;

    public EventSpecification(EventEntity filter) {
        super();
        this.filter = filter;
    }

    @Override
    public Predicate toPredicate(Root<EventEntity> root, CriteriaQuery<?> criteriaQuery,
                                 CriteriaBuilder criteriaBuilder) {

        Predicate predicate = criteriaBuilder.disjunction();

        if (filter.getAccount() != null) {
            predicate.getExpressions()
                    .add(criteriaBuilder.equal(root.get("account"), filter.getAccount()));
        }

        if (filter.getName() != null) {
            predicate.getExpressions()
                    .add(criteriaBuilder.equal(root.get("name"), filter.getName()));
        }
        if (filter.getLanguage() != null) {
            predicate.getExpressions()
                    .add(criteriaBuilder.equal(root.get("language"), filter.getLanguage()));
        }
        if (filter.getDateFormat() != null) {
            predicate.getExpressions()
                    .add(criteriaBuilder.equal(root.get("dateFormat"), filter.getDateFormat()));
        }
        if (filter.getTimeFormat() != null) {
            predicate.getExpressions()
                    .add(criteriaBuilder.equal(root.get("timeFormat"), filter.getTimeFormat()));
        }
        if (filter.getTimeZone() != null) {
            predicate.getExpressions()
                    .add(criteriaBuilder.equal(root.get("timeZone"), filter.getTimeZone()));
        }

        return predicate;
    }

}
