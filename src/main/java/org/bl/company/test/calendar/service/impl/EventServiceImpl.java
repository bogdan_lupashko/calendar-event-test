package org.bl.company.test.calendar.service.impl;

import lombok.RequiredArgsConstructor;
import org.bl.company.test.calendar.exception.GenericException;
import org.bl.company.test.calendar.repo.EventRepository;
import org.bl.company.test.calendar.repo.EventSpecification;
import org.bl.company.test.calendar.repo.entity.EventEntity;
import org.bl.company.test.calendar.service.EventService;
import org.bl.company.test.calendar.service.dto.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EventServiceImpl implements EventService {

    private final EventRepository eventRepository;

    @Override
    public Page<Event> searchCalendar(Event event, Pageable pageable) {
        return Optional.of(event)
                .map(this::mapEventToEventEntity)
                .map(EventSpecification::new)
                .map(spec -> eventRepository.findAll(spec, pageable))
                .orElseThrow(() -> new GenericException("Calendar not found."))
                .map(this::mapEventEntityToEvent);
    }

    @Override
    public Event create(Event event) {
        return Optional.of(event)
                .map(this::mapEventToEventEntity)
                .map(eventRepository::save)
                .map(this::mapEventEntityToEvent)
                .orElseThrow(() -> new GenericException("Calendar couldn't be created."));
    }

    private EventEntity mapEventToEventEntity(Event event) {
        return EventEntity.builder()
                .account(event.getAccount())
                .dateFormat(event.getDateFormat())
                .timeFormat(event.getTimeFormat())
                .timeZone(event.getTimeZone())
                .language(event.getLanguage())
                .name(event.getName())
                .build();
    }

    private Event mapEventEntityToEvent(EventEntity eventEntity) {
        return Event.builder()
                .account(eventEntity.getAccount())
                .dateFormat(eventEntity.getDateFormat())
                .timeFormat(eventEntity.getTimeFormat())
                .timeZone(eventEntity.getTimeZone())
                .language(eventEntity.getLanguage())
                .name(eventEntity.getName())
                .build();
    }
}
