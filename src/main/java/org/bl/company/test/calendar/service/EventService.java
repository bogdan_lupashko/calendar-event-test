package org.bl.company.test.calendar.service;

import org.bl.company.test.calendar.service.dto.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EventService {

    Page<Event> searchCalendar(Event calendar, Pageable pageable);

    Event create(Event calendar);

}
