package org.bl.company.test.calendar.exception;

public class GenericException extends RuntimeException {

    public GenericException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public GenericException(String msg) {
        super(msg);
    }

    public GenericException(Throwable cause) {
        super(cause);
    }
}